![Screenshot preview of the theme "Guardian Deity" by glenthemes](https://68.media.tumblr.com/15ca24bece5a51e2ed72fb9d2040f113/tumblr_ohkm2kUBBt1ubolzro1_540.gif)

**Theme no.:** 10  
**Theme name:** Guardian Deity  
**Theme type:** Free / Tumblr use  
**Description:** This theme was previously called "Revival", but is now revamped as "Guardian Deity" and features Nishinoya Yuu from Haikyuu!!.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-10-11](https://64.media.tumblr.com/cc97aea35e511c1ad513143195ae68d5/tumblr_nw2w758qQp1ubolzro1_540.gif)  
**Rework date:** 2017-01-12

**Post:** [glenthemes.tumblr.com/post/155745166529](https://glenthemes.tumblr.com/post/155745166529)  
**Preview:** [glenthpvs.tumblr.com/noyah70](https://glenthpvs.tumblr.com/noyah70)  
**Download:** [pastebin.com/wLYRPGAk](https://pastebin.com/wLYRPGAk)
