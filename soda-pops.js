document.addEventListener("DOMContentLoaded", () => {
    let npf_auds = document.querySelectorAll(".npf-audio-details");
    npf_auds?.forEach(n => {
        let z = n.querySelectorAll("span[class$='label']");
        z?.forEach(y => {
            y.textContent = y.innerHTML.trim().toString()
            y.style.marginRight = "3px"
        })
    })
})
